Source: surgescript
Section: libs
Priority: optional
Maintainer: Debian Games Team <pkg-games-devel@lists.alioth.debian.org>
Uploaders: Carlos Donizete Froes <coringao@riseup.net>
Build-Depends: cmake, debhelper-compat (= 13)
Standards-Version: 4.5.1
Rules-Requires-Root: no
Homepage: https://docs.opensurge2d.org
Vcs-Browser: https://salsa.debian.org/games-team/surgescript
Vcs-Git: https://salsa.debian.org/games-team/surgescript.git

Package: surgescript
Section: games
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}
Description: Scripting language for games
 Unleash your creativity! With SurgeScript, you unleash your creativity
 and create your own amazing interactive content.
 .
 Unlike other programming languages, SurgeScript is designed with the specific
 needs of games in mind.
 .
 Easy for beginners, powerful for experts. Object-oriented, dynamically typed
 and based on state machines.
 .
 These features come from the experience of the developer dealing with
 game engines, applications related to computer graphics and so on.
 .
 Some of the best practices have been incorporated into the language itself,
 making things really easy for developers and modders.

Package: libsurgescript0.5.4.4
Architecture: any
Multi-Arch: same
Depends: ${misc:Depends}, ${shlibs:Depends}
Description: Scripting language for games (library files)
 Unleash your creativity! With SurgeScript, you unleash your creativity
 and create your own amazing interactive content.
 .
 Unlike other programming languages, SurgeScript is designed with the specific
 needs of games in mind.
 .
 Easy for beginners, powerful for experts. Object-oriented, dynamically typed
 and based on state machines.
 .
 These features come from the experience of the developer dealing with
 game engines, applications related to computer graphics and so on.
 .
 Some of the best practices have been incorporated into the language itself,
 making things really easy for developers and modders.
 .
 This package contains shared runtime libraries.

Package: libsurgescript-dev
Section: libdevel
Architecture: any
Breaks: surgescript (<< 0.5.4.4)
Replaces: surgescript (<< 0.5.4.4)
Depends: libsurgescript0.5.4.4 (= ${binary:Version}), ${misc:Depends}
Description: Scripting language for games (development files)
 Unleash your creativity! With SurgeScript, you unleash your creativity
 and create your own amazing interactive content.
 .
 Unlike other programming languages, SurgeScript is designed with the specific
 needs of games in mind.
 .
 Easy for beginners, powerful for experts. Object-oriented, dynamically typed
 and based on state machines.
 .
 These features come from the experience of the developer dealing with
 game engines, applications related to computer graphics and so on.
 .
 Some of the best practices have been incorporated into the language itself,
 making things really easy for developers and modders.
 .
 This package contains the header files and static library needed to
 compile applications that use libsurgescript.
